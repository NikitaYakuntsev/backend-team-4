package ru.surf.hackathon.backend.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@EnableScheduling
public class ScheduleConfig {

    @Value("${lamptest.base-url}")
    String baseUrl;

    @Bean
    @Qualifier("LamptestWebClient")
    public WebClient webClient() {
        return WebClient.create(baseUrl);
    }
}
