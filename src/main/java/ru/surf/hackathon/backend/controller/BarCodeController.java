package ru.surf.hackathon.backend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.surf.hackathon.backend.service.logic.impl.LampServiceImpl;
import ru.surf.hackathon.backend.service.model.LampDto;

@RestController
@RequestMapping("/api/v1/lamp")
@RequiredArgsConstructor
public class BarCodeController {
    private final LampServiceImpl lampService;

    @GetMapping("/{barcode_id}")
    public ResponseEntity<LampDto> getLampById(@PathVariable("barcode_id") String barcodeId) {
        return ResponseEntity.ok(lampService.getLampById(barcodeId));
    }
}
