package ru.surf.hackathon.backend.persistence.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.surf.hackathon.backend.service.model.enums.LightIndicator;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "lamps")
public class LampEntity {
    @Id
    @Column(name = "barcode_id", nullable = false, length = 13)
    private String barcodeId;
    private String brand;
    private String model;
    @Column(name = "lamp_description")
    private String lampDescription;
    @Column(name = "price_in_rub")
    private Double priceInRub;
    @Column(name = "price_in_usd")
    private Double priceInUsd;
    private Double power;
    private Integer lm;
    private Double efficiency;
    private Integer equivalent;
    @Column(name = "color_temp")
    private Integer colorTemp;
    private Double cri;
    private Integer angle;
    @Column(name = "flicker_factor")
    private Integer flickerFactor;
    @Column(name = "work_with_backlight_indicator")
    @Enumerated(value = EnumType.STRING)
    private LightIndicator workWithBacklightIndicator;
    private Double rating;
    private Integer warranty;
    private Boolean actuality;
}