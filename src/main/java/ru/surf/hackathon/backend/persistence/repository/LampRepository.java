package ru.surf.hackathon.backend.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.surf.hackathon.backend.persistence.entity.LampEntity;

public interface LampRepository extends JpaRepository<LampEntity, String> {
}
