package ru.surf.hackathon.backend.service.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class LampNotFoundException extends RuntimeException {

    private final HttpStatus status;
    private final String message;

    public LampNotFoundException(String message, HttpStatus status) {
        super(message);
        this.status = status;
        this.message = message;
    }
}
