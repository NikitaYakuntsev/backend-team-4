package ru.surf.hackathon.backend.service.exception.handler;

import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.surf.hackathon.backend.service.exception.LampNotFoundException;

@ControllerAdvice
public class LampExceptionHandler {
    @ExceptionHandler(LampNotFoundException.class)
    public ResponseEntity<String> handleModelException(LampNotFoundException e) {
        return ResponseEntity
                .status(e.getStatus())
                .body(e.getMessage());
    }
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleModelException(ConstraintViolationException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Validation error");
    }
}
