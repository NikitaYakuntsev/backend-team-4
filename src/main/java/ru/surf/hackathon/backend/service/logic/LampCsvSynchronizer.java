package ru.surf.hackathon.backend.service.logic;

import org.springframework.scheduling.annotation.Scheduled;

public interface LampCsvSynchronizer {
    @Scheduled(fixedDelay = 86400)
    void synchronizeDbWithCsv();

}
