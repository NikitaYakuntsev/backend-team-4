package ru.surf.hackathon.backend.service.logic;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import org.springframework.validation.annotation.Validated;
import ru.surf.hackathon.backend.service.model.LampDto;

@Validated
public interface LampService {
    LampDto getLampById(
            @NotBlank
            @Size(min = 13, max = 13)
            String barcodeId
    );
}
