package ru.surf.hackathon.backend.service.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.surf.hackathon.backend.persistence.entity.LampEntity;
import ru.surf.hackathon.backend.persistence.repository.LampRepository;
import ru.surf.hackathon.backend.service.logic.LampCsvSynchronizer;
import ru.surf.hackathon.backend.service.mapper.LampMapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

@Service
public class LampCsvSynchronizerImpl implements LampCsvSynchronizer {
    @Qualifier("LamptestWebClient")
    private final WebClient webClient;
    private final LampMapper lampMapper;
    private final LampRepository lampRepository;
    @Value("${lamptest.file-url}")
    String fileUrl;
    @Value("${lamptest.local-file-destination}")
    String destination;

    @Autowired
    public LampCsvSynchronizerImpl(WebClient webClient, LampMapper lampMapper, LampRepository lampRepository) {
        this.webClient = webClient;
        this.lampMapper = lampMapper;
        this.lampRepository = lampRepository;
    }


    @Override
    @Scheduled(fixedDelay = 86400)
    public void synchronizeDbWithCsv() {
//        need to uncomment

//        fetchCsvFile();
//        processCsvFile();
    }

    private void fetchCsvFile() {
        Flux<DataBuffer> fileFlux = webClient.get().uri(fileUrl).retrieve().bodyToFlux(DataBuffer.class);

        Path path = Paths.get(destination);
        DataBufferUtils.write(fileFlux, path, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)
                .block();
    }

    private void processCsvFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(destination))) {
            br.readLine();
            String line;
            List<LampEntity> lamps = new ArrayList<>(100);
            while ((line = br.readLine()) != null) {
                String[] arr = line.split(";");
                LampEntity lampEntity = lampMapper.stringArrToLampEntity(arr);
                if (lampEntity != null) {
                    lamps.add(lampEntity);
                }
                if (lamps.size() > 100) {
                    lampRepository.saveAll(lamps);
                    lamps.clear();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
