package ru.surf.hackathon.backend.service.logic.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.surf.hackathon.backend.service.exception.LampNotFoundException;
import ru.surf.hackathon.backend.service.mapper.LampMapper;
import ru.surf.hackathon.backend.service.model.LampDto;
import ru.surf.hackathon.backend.persistence.repository.LampRepository;
import ru.surf.hackathon.backend.service.logic.LampService;

@Service
@RequiredArgsConstructor
public class LampServiceImpl implements LampService {

    private final LampRepository lampRepository;
    private final LampMapper lampMapper;

    public LampDto getLampById(String barcodeId) {
        return lampMapper.lampToLampDto(
                lampRepository
                        .findById(barcodeId)
                        .orElseThrow(() -> {throw new LampNotFoundException("Lamp not found", HttpStatus.NOT_FOUND);}));
    }
}
