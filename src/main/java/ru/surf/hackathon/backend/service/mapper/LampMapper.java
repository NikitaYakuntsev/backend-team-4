package ru.surf.hackathon.backend.service.mapper;

import org.mapstruct.Mapper;
import ru.surf.hackathon.backend.persistence.entity.LampEntity;
import ru.surf.hackathon.backend.service.model.LampDto;
import ru.surf.hackathon.backend.service.model.enums.LightIndicator;

@Mapper(componentModel = "spring")
public abstract class LampMapper {
    public abstract LampDto lampToLampDto(LampEntity lamp);

    public LampEntity stringArrToLampEntity(String[] line) {
        if (line[20].length() != 13) return null;

        LampEntity lamp = new LampEntity();

        try {
            lamp.setBarcodeId(line[20]);
            lamp.setBrand(line[1]);
            lamp.setModel(line[2]);
            lamp.setLampDescription(line[56]);
            lamp.setPriceInRub(line[28].length() > 0 ? Double.valueOf(line[28]) : null);
            lamp.setPriceInUsd(line[29].length() > 0 ? Double.valueOf(line[29]) : null);
            lamp.setPower(line[30].length() > 0 ? Double.valueOf(line[30]) : null);
            lamp.setLm(line[32].length() > 0 ? Integer.valueOf(line[32]) : null);
            Integer lm = lamp.getLm();
            Double p = lamp.getPower();
            double eff = 0.0;
            if (lm != null && p != null) {
                eff = lm / p;
            }
            lamp.setEfficiency(eff);
            lamp.setEquivalent(line[52].length() > 0 ? Integer.valueOf(line[52]) : null);
            lamp.setColorTemp(line[33].length() > 0 ? Integer.valueOf(line[33]) : null);
            lamp.setCri(line[34].length() > 0 ? Double.valueOf(line[34]) : null);
            lamp.setWorkWithBacklightIndicator(
                    LightIndicator.valueOfOrdinal(
                            line[41].length() > 0 && !line[41].equals("0") ?
                                    Integer.valueOf(line[41]) :
                                    null));
            lamp.setRating(line[53].length() > 0 ? Double.valueOf(line[53]) : null);
            lamp.setWarranty(line[14].length() > 0 && !line[14].equals("-") ? Integer.valueOf(line[14]) : null);
            lamp.setActuality(line[54].equals("1"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            lamp = null;
        }

        return lamp;
    }
}
