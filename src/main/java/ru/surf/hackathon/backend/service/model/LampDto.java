package ru.surf.hackathon.backend.service.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LampDto {
    private String brand;
    private String model;
    private String lampDescription;
    private Double priceInRub;
    private Double priceInUsd;
    private Double power;
    private Integer lm;
    private Double efficiency;
    private Integer equivalent;
    private Integer colorTemp;
    private Double cri;
    private Integer angle;
    private String workWithBacklightIndicator;
    private Integer flickerFactor;
    private Double rating;
    private Integer warranty;
    private Boolean actuality;
}
