package ru.surf.hackathon.backend.service.model.enums;

public enum LightIndicator {
    WORK,
    WEAK_LIGHT,
    NOT_WORK;

    public static LightIndicator valueOfOrdinal(Integer index) {
        return index != null ? values()[index-1] : null;
    }
}
