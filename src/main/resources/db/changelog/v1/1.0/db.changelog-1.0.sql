CREATE TABLE lamps
(
    barcode_id                    VARCHAR(13),
    brand                         TEXT,
    model                         TEXT,
    lamp_description              TEXT,
    price_in_rub                  REAL,
    price_in_usd                  REAL,
    power                         REAL,
    lm                            INTEGER,
    efficiency                    REAL,
    equivalent                    INTEGER,
    color_temp                    INTEGER,
    cri                           REAL,
    angle                         INTEGER,
    flicker_factor                INTEGER,
    work_with_backlight_indicator TEXT,
    rating                        REAL,
    warranty                      INTEGER,
    actuality                     BOOLEAN
);